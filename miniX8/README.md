##### Links
[Executable](https://lentz.gitlab.io/aestheticprogramming/miniX8/)

[Repository](https://gitlab.com/lentz/aestheticprogramming/-/blob/main/miniX8/miniX8.js)

![](miniX8.gif)

### Title: Pokemon personality test

##### **What we made**
Our work is a Pokemon personality test that decides which Pokemon you are based on your given answers. We have created XX amount of questions with varying amounts of answers. Once you’ve answered all the questions, you’re given one of 12 specially curated Pokemon. Each Pokemon is specifically chosen for some of its most prevalent personality traits. For example, did we choose Snorlax for his laziness trait and Psyduck and Magikarp because they are absolutely useless and panic over the smallest things. We thought it was funny to create some kind of personality test and everyone in our group likes Pokemon so we thought that could be a fun personality test, especially because some Pokemon have very recognizable and significant traits themselves. We only used the first generation of Pokemon to make it relatable for us and our approximate age group. We also choose this generation to add a nostalgic vibe and feel to the quiz because we hope more people will take the quiz.

##### **How it works**
Our .JSON-file contains objects that consist of the given Pokemons name, and then a numeric ‘points’-string that denotes the amount of points your answers award to each Pokemons personality/appearance traits. As the assignment required the use of a JSON file, we had to use ```loadJSON()``` in a ```preLoad()``` function for the first time.  For other newer syntaxes, we used ```hide()``` to hide the buttons, and ```indexOf()``` to get the right picture to display in the end. We also used a lot of array functionality with the .(dot) and some Math was used to get the highest scoring pokemon. The 2 JSON themselves are built up with an array with objects inside of them. The names.json have objects with the name and points total in them, and the questions.json has objects with the questions, the answers, which are inside yet another array(so an array inside of an array), and all the points tables, which are also inside another array, and are there to add to each name’s points total.

##### **Further explanation**
The miniX is text-based and interactive. The reader forges their own path through the question and end up at a personalized result. The whole thing is structured like a questionaire and similar to a "Buzz Feed"-quiz. Therefore the medium it resembles the most is a digitalized tabloid. Two .json files are used to store information. These are build after the concepts explained in Daniel Shiffman's p5.js tutorial video series "10.2: What is JSON?".
Through the questions, made by code, the reader is actively reflecting on their own bevaviour. Because the answers are judged in the end to decide which Pokemon best represents themselves, the user could be influenced in their decision making, with the objective of being judged a certain way. This is also part of the reason why our idea had to be e-lit, as opposed to a paper sheet were you tick boxes and count your questions. In the case of a purely text based survey, there would have to be some kind of explanation for which answer gives what kind of result, which our approach does not have. This makes it a lot harder for the user to cheat. Someone trying to trick our survey would have to take it numerous times, and write down the results a certain chain of answers have given each time.

##### **Side note**
This miniX, especially the programming side, took A LOT of time, at least twice as long as any of our previous individual assignments. The reasons for this are probably a mix of higher complexity of our program through the use of new syntax like .json, as well as the fact, that this was the first time we worked on a miniX as a group. Discussions about what the content should be took up a considerable amount of time, and we think through this discussion we ended up with a superior product.

##### **Reference**

Daniel Shiffman, “10.2: What is JSON? Part I - p5.js Tutorial” (2017), https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r.

Daniel Shiffman, “10.2: What is JSON? Part II - p5.js Tutorial” (2017), https://www.youtube.com/playlist?list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r.

