//miniX7 - Revisit the past (Revision of miniX4 - Data capture)
//with code from class05
//powered with clmtrackr by Audun Mathias Øygard
//by Linus Lentz

let capture;
let ctracker;
let mic;
let recorder;
let soundfile;
let fft;
let button;
let song;

function setup() {
  //create canvas
  createCanvas(windowWidth, windowHeight);
  background(0);

  //rickroll
  song = loadSound('rickroll.m4a');

  //setup web cam capture
  capture = createCapture(VIDEO);
  capture.size(windowWidth, windowHeight);
  capture.hide();

  //face ctracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

  //microphone
  mic = new p5.AudioIn();
  mic.start();
  fft = new p5.FFT();
  fft.setInput(mic);

  //button
  button = createButton('press "space" to exit');

  noStroke();
}


function draw(){
  button.position(0, 0);

  //activate rickroll
  if (keyCode === 32) {
    if (!song.isPlaying()) {
      song.play();
    }
  }

  background(0);

  //draw video
  image(capture, 0, 0, windowWidth, windowHeight);

  //text
  text('Thank you for your voice and face 💸💸💸', windowWidth/3, windowHeight/8);
  textAlign(CENTER);
  textSize(48);

  let positions = ctracker.getCurrentPosition();

  //draw points that track the face
  for (let i = 0; i < positions.length; i++) {
     noStroke();
     //color with alpha value
     fill(255,0,0,180);
     //draw ellipse at each position point
     ellipse(positions[i][0], positions[i][1], 7, 7);
  }

  let waveform = fft.waveform();
  let spectrum = fft.analyze();

  //visualize microphone input
  noStroke();
  fill(255);
  for (let i = 0; i< spectrum.length; i++){
    let x = map(i, 0, spectrum.length, 0, width*8);
    let h = -height + map(spectrum[i], 0, 255, height, 0);
    rect(x, height, width / spectrum.length, h )
 }
}
