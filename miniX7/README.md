
<h2>README for miniX7 by Linus Lentz</h2>

![](miniX7.gif)

Original miniX: https://gitlab.com/lentz/aestheticprogramming/-/tree/main/miniX4

Run miniX7: https://lentz.gitlab.io/aestheticprogramming/miniX7/

Source code for miniX7: https://gitlab.com/lentz/aestheticprogramming/-/blob/main/miniX7/sketch.js


<h4>The assignment</h4>
I chose to rework my fourth miniX of which the theme was data capture. I choose this one, because it is the miniX I was least satisfied with when I looked through all of them again. When I saw the old miniX, I thought it had much more potential, which I could not really build upon, as some parts of the library would not work on my computer at the time. I got these working now, so it was time to revisit. The original miniX4 was very simple. Basically it was the users webcam projected on their screen, and as soon as the clmtracker face tracking library would detect a face the program would freeze, making it seem like a photograph was taken. Part of the assignment for  week four was to write an application to the Transmediale 2015 festival. I advertised the miniX as as a disruption for data theft using the webcam. The thing I wanted to achieve with the rework was to add more data inputs and to change the part of the code that crashes it. The crash was funny enough when I found it, but I came to the conclusion that crashing your code purposefully is generally not a good practice. 


<h4>Changes to the miniX</h4>
The whole miniX was basically reworked from scratch. The screenshot feature was cut to make room for new ones. That means the purposeful crash was completely removed. I kept the cracker and made it show the points on the users face. The point with this is to make the user think that the face is analyzed, downloaded and stored. The next data input I added was microphone input which would not work the last time. I managed to make the frequency of the input show on screen with the help of p5.FFT(), FFT stands for Fast Fourier Transform. This function uses an analyzing algorithm for sound and makes it possible to visualize them on screen as sound waves. The reason for its inclusion was also to trick the user into thinking that their voice is being analyzed and stored. I also added a dummy button, which says, that to stop the program, the user has to press the space key. The joke is, that instead it plays Rick Astleys song „Never gonna give you up“, which is an internet meme used when tricking people. This rounds of the program, which purpose it is to trick the user into thinking their data is stolen.


<h4>Conclusion</h4>
While reworking the miniX, I learned how to visualize sound in P5.js, which is something I wanted to learn since making miniX4. I again learned the importance of order in the draw function, for keeping the layers in the right order, like having text appear over the video and the background refresh below it and so on. Like Ulises A. Mejias and Nick Couldry describe in their article „Datafication“, the process of datafication is turning something into data. In this case sound for example is turned into waves and these again consist of code which at it’s base is made of zeroes and ones. Mentioned in the article is also, that this process of datafication is often tied to „big data“, meaning the large data corporations like for example Google and Facebook. This is that my miniX partly tries to refer to, in that it takes data from the user and turns it into data in the form of a face model or visualization of their voice. These could be used by corporations to turn out a profit.
Programming and digital culture are deeply intertwined. You would not have digital culture without programming. Because of this, the person that is able to program is able to influence digital culture. My minix is a visual representation of how captured data can be visualized, and what the computer alone is able to record of us. At the same time it is supposed to be fun.



Reference:

Rick Astley - Never gonna give you up (1988): https://www.youtube.com/watch?v=dQw4w9WgXcQ

Fast Fourier Transform analyzing algortithm: https://p5js.org/reference/#/p5.FFT

Transmedial call for works 2015: https://archive.transmediale.de/content/call-for-works-2015

Aesthetic programming class for miniX4: https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/class05

Ulises A. Mejias and Nick Couldry, “Datafication,” Internet Policy Review 8.4 (2019), https://policyreview.info/concepts/datafication.
