function setup () {
  // Using the refresh rate of the GameBoy
  frameRate(59.7)
  createCanvas(800,800);
  background(255);
  // Drawing body
  rect(30, 20, 220, 350);
  // Drawing the screen (resolution: w160, h144)
  rect(60, 45, 160, 144);
  //Drawing the buttons
  //A
  circle(215, 250, 35);
  //B
  circle(170, 270, 35);
  //Start
  ellipse(162, 350, 30, 10);
  //Select
  ellipse(122, 350, 30, 10);
  //Left & Right
  rect(50, 250, 70, 25);
  //Up & Down
  rect(72, 226, 25, 70);


}

function draw () {

  /*
  Making the visual presentation and interface for a GameBoy software emulator
  */

}
