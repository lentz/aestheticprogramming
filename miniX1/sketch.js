// miniX 1: "The future of Retro Gaming?"
// by Linus Lentz

let vid;
function setup() {

// Creating the canvas and background
createCanvas(800,800);
background(255);

// Drawing the body
fill(220, 220, 228);
rect(60, 40, 440, 700);

//Drawing the buttons
//A
fill(205, 0, 100);
circle(430, 500, 70);

//B
fill(205, 0, 100);
circle(340, 540, 70);

//Start
fill(136, 136, 136);
ellipse(324, 700, 60, 20);

//Select
fill(136, 136, 136);
ellipse(244, 700, 60, 20);

//Left & Right
fill(51);
rect(100, 500, 140, 50);

//Up & Down
rect(144, 452, 50, 140);

//Creating the video output
vid = createVideo(
  ['zelda.mp4'],
  vidLoad
);

vid.position(120, 90);
vid.size(320, 288);
}

// This function is called when the video loads
function vidLoad() {
vid.loop();
vid.volume(0);

// text
fill(50);
textSize(25);
textAlign(CENTER);
text('The future ', 650, 200);
textSize(35);
text('of', 650, 300);
textSize(40);
text('Retro Gaming', 650, 400);
textSize(80);
text('?', 650, 510);
}
