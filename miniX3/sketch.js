//miniX3 by Linus lentz
//loadingRave
//based on Winnie Soon's throbber:
//https://gitlab.com/lentz/aestheticprogramming/-/blob/main/miniX3/original_throbber_by_siusoon.js

let r = 0; //Part 1 of setting up the color changing color values
let g = 0;
let b = 0;

let img;
function preload() {
  img = loadImage('rave.jpg');
}
function setup() {
 createCanvas(1280, 960);
 frameRate (30); //this has influence on how fast the throbber spins
}

function draw() {
  background(img); //setting the rave as background image
  colors();
  drawElements();
  cursor('grab'); //changing the mouse cursor to a hand

}

function colors(x, y){ //Part 2 of setting up the color changing color values
  r = r + 2;
  if (r > 255) {
    r = 0; //making it so that the color resets instead of getting stuck at value 255
  }
  g = g + 1;
  if (g > 255) {
    g = 0;
  }
  b = b + 0.5;
  if (b > 255) {
    b = 0;
  }
}

function drawElements(x, y) { //(x, y) so that the function is only activated when the mouse is moving
  if(x&&y){ //setting the condition for function drawElements
  let num = 20;
  push();
  translate(pmouseX, pmouseY); //making the throbber follow the mouse postion
  let cir = 360/num*(frameCount%num); //setup rotation
  rotate(radians(cir)); //make rotate
  noStroke();
  fill(r, g, b); //making the throbber change colors
  rect(5, 5, 180, 20); //setting the throbbers shape
  pop();
}
 }

function mouseMoved() {
   drawElements(true,true); //Core function that makes the throbber only spins when the mouse is moved
}
