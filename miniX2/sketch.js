//miniX 2 - Emoji
//by Linus Lentz
//contains code orignally written by maxremfort

  //Setting up the canvas and the painting "Ultima Cena" by Juan de Juanes
let img;
function preload() {
  img = loadImage('ultimacena.jpg');
}

function setup() {
  createCanvas(1920, 1215);
  image(img, 0, 0);
}

  //The upcoming emoji is enabled
  //Code written by maxremfort
function draw() {
//background(220);
  fill(220)
 //rect(0,0,400,400)
mouseClicked();
}

  //The first emoji is made and prepared
  //Original code by maxremfort edited by me
function myemoji(x,y){
 if(x&&y){

    //Face
    fill(255,203,76); //emoji yellow
    ellipse(pmouseX, pmouseY, 100, 100);

    //Left eye
    fill(30); //dark gray
    rect(pmouseX-20, pmouseY-10, 25, 5, 20); //eyebrow
    fill(255);
    circle(pmouseX-5, pmouseY+5, 20); //eye
    fill(0);
    circle(pmouseX-5, pmouseY+5, 10) //pupil

    //Right eye
    fill(30) //dark gray
    rect(pmouseX+20, pmouseY-10, 25, 5, 20); //eyebrow
    fill(255);
    circle(pmouseX+30, pmouseY+5, 20); //eye
    fill(0)
    circle(pmouseX+30, pmouseY+5, 10); //pupil


    //Mouth
    fill(255);//light pink
    arc(pmouseX, pmouseY+20, 30, 30, 0, radians(180), PIE);

 }

  }

  //The second emoji is prepared
function haloemoji(x,y){
 if(x&&y){

    //emoji
    textSize(100);
    textAlign(CENTER);
    text('😇', pmouseX, pmouseY+30);

 }

  }

  //The secret third emoji is prepared
function secretemoji(x,y){
 if(x&&y){

    //emoji
    textSize(100);
    textAlign(CENTER);
    text('😂', pmouseX, pmouseY+30);
 }

  }

  //The emoji is deployed with either a right, middle or left mouseclick
  //Orginal code by maxremfort edited by me

function mouseClicked(){

 if(mouseIsPressed){

    if (mouseButton === LEFT)
     myemoji(true,true);

     if (mouseButton === RIGHT)
      haloemoji(true,true);

      if (mouseButton === CENTER)
       secretemoji(true,true);
 }

}
