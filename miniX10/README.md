
![](pix2pix.png)

### miniX10 - Machine unlearning

##### pix2pix homepage: https://learn.ml5js.org/#/reference/pix2pix

### Which sample code have you chosen and why?

We chose the code sample Pix2pix. We thought it was a nice and funny program. It was a new sample of code that none of us had experienced before. We found the code when we were searching for something new and thought it was funny that Pikachu was the main character. We had already created a code where we created a questionnaire which should figure out which Pokemon the user who took the test were. 
 
### Have you changed anything in order to understand the program? If yes, what are they?

We changed some of the code from lines 93 and 94. We wanted to investigate the ‘stroke weight’ and also the color to see how the program/code transferred what we drew in the sketching area and how the outcome would look like if we changed the weight of the stroke. It was very difficult to draw a Pikachu when we had the stroke weight set to 10 pixels but easier to manage and actually draw a “nicer” picture of Pikachu when it was at 1 pixel. We looked into another code because this Pix2pix code was very complicated and complex. 

The other code is the CharRNN_Text. We played with the slider to understand how the complexity of the sentences were created if the slider was set to more than 1 or lower than 0,5. 

 
### Which line(s) of code is particularly interesting to your group, and why?

We find it interesting that the Pix2pix program is class-based.


We found the entire code to be quite difficult to comprehend. This is why we came upon a new code that we wanted to look deeper into. The other option we considered was the CharRNN Text code. This code sample was more understandable and straightforward to comprehend. We found it more enjoyable to utilize this code since it was more enjoyable to erase and add new elements to the code, making it easier to comprehend what we could and couldn't do.
 
### How would you explore/exploit the limitation of the sample code/machine learning algorithms?

For Pix2pix one way to explore the sample code is to train the code on different picture sets. You could test out different Pokemon or different real life objects to see what happens in the program. You could also try to see what happens if you provide pictures of groups of people with different skin colours. How would the sample code make distinctions between what skin colour to give your drawing and based on what? This also touches on the samples themselves and what you feed into the code and how that could be potentially harmful.  


 
### What are the syntaxes/functions that you don't know before? What are they and what have you learnt?

There are plenty of new syntaxes and functions in this pix2pix code. All of the TensorFlow-coding is totally new to all of us. We couldn’t pick only one, but we wanted to emphasize the Tensorflow-coding and the HTML coding. It was difficult to understand a lot of this code which is the reason why we wanted to look into something a bit simple. We chose the CharRNN_Text code and it became evident to us on line 20 of the HTML-index file that if we wanted a phrase that made sense, we should maintain the slider at 0,5 but if we wanted something crazy with a lot of symbols, we should set the slider above 1 or set it to a negative number. This would create a lot of different symbols and weird letters. 

 
### How do you see a border relation between the code that you are tinkering with and the machine learning applications in the world (e.g creative AI/ voice assistants/driving cars, bot assistants, facial/object recognition, etc)?

Like other AIs, Pix2pix does make mistakes, or at the very least very questionable decisions, at times. It is fair to say though, that it is not always the AIs fault when something goes wrong. In cases where we made a bad effort of drawing Pikachu, it got clear that the AI was struggling to fill in the colors properly, and one may blame the artist in this case. But even when the outlines were nicely drawn, there always a kind of creepyness left after coloring in. We identified a reason for this, mainly that the colors do not have clean edges, but bleed into each other. Such inaccuracy manifests itself in other AIs too. Like when a navigation system shows a way over non-existent road. Or when a voice assistant understands a similar word instead of the one the user said.

 
### What do you want to know more/ What questions can you ask further?

We are wondering about, how a Pix2Pix for human characters would look like. With the AIs current capability it would probably look similarly creepy. With an improved AI, the coloring might even approach an uncanny valley, where coloring would look close enough, but also off at the same time. 
It is interesting to think of how many source images that are required for there to be an “accurate” result for different things. Like, one would assume that something complex like the recognition of a human face would require an extreme amount of learning, whereas maybe something a little simpler like Minecraft blocks(??) might require less. The specific amount of when it goes from bad to usable is interesting. 



