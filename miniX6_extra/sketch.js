//miniX6 by Linus Lentz

//Oh no! A Goomba has stolen Marios hat!
//He can't show up to Peach's castle like this... :(
//Can you help Mario get his hat back?

//**rules**
//move mario with the w,a,s,d or arrow keys
//dodge the goomba
//catch the hat

//establishing all needed values
let hxoff = 0.0;
let hyoff = 1.0;
let gxoff = 0.0;
let gyoff = 1.0;
let mx = 100;
let my = 100;
let hx;
let hy;
let gx;
let gy;
let img;
let baldMario;
let hat;
let goomba;
let hatMario;
let deadMario
let hathit = false;
let goombahit = false;

//importing all assets
function preload() {
  backdrop = loadImage('sprites/backdrop.png');
  win = loadImage('sprites/win.png');
  loose = loadImage('sprites/loose.png');
  baldMario = loadImage('sprites/baldMario.png');
  deadMario = loadImage('sprites/deadMario.png');
  hat = loadImage('sprites/hat.png');
  goomba = loadImage('sprites/goomba.png');
  hatMario = loadImage('sprites/hatMario.png');
  castle = loadSound('sounds/castle.mp3');
  success = loadSound('sounds/success.mp3');
  gameover = loadSound('sounds/gameover.mp3');
}

//creating the canvas, importing the background and starting the music
function setup() {
  createCanvas(1000, 600);
  background(backdrop);
  castle.play();
}

function draw() {
  background(backdrop);
  image(baldMario, mx, my);

  //creating the values for hat and goomba movements
  hxoff = hxoff + 0.01;
  hyoff = hyoff - 0.01;
  gxoff = gxoff + 0.015;
  gyoff = gyoff - 0.015;

  //creating the condition for picking up the hat
  if(hathit === false) {
    hx = noise(hxoff) * width;
    hy = noise(hyoff) * height;
    image(hat, hx, hy);
  }

  //creating the condition for getting hit by the goomba
  if(goombahit === false) {
    gx = noise(gxoff) * width;
    gy = noise(gyoff) * height;
    image(goomba, gx, gy);
  }

  //measuring the distance between mario and the hat and executing the conditions
  hd = dist(mx, my, hx, hy)
  if(hd < 100) {
    image(hatMario, mx, my);
    image(win, 0, 0);
    hathit = true;
    hx = mx;
    hy = my;
    gx = -1000;
    gy = -1000;
    castle.stop();
    if (!success.isPlaying()) {
      success.play();
      gameover.stop();
    }
  }

  //measuring the distance between mario and the goomba and executing the conditions
  gd = dist(mx, my, gx, gy)
  if(gd < 100) {
    image(deadMario, mx, my);
    image(loose, 0, 0);
    goombahit = true;
    gx = mx;
    gy = my;
    hx = -1000;
    hy = -1000;
    castle.stop();
    if (!gameover.isPlaying()) {
      gameover.play();
      success.stop();
    }
  }

  //making mario able to be controlled
  if (keyIsDown(LEFT_ARROW)) {
    mx -= 5;
  }

  if (keyIsDown(RIGHT_ARROW)) {
    mx += 5;
  }

  if (keyIsDown(UP_ARROW)) {
    my -= 5;
  }

  if (keyIsDown(DOWN_ARROW)) {
    my += 5;
  }
  if (keyIsDown(65)) {
    mx -= 5;
  }

  if (keyIsDown(68)) {
    mx += 5;
  }

  if (keyIsDown(87)) {
    my -= 5;
  }

  if (keyIsDown(83)) {
    my += 5;
  }
}
