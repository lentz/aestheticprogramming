![](miniX5.gif)

**WARNING! FLASHING LIGHTS!**

RUNME: https://lentz.gitlab.io/aestheticprogramming/miniX5/

Source Code: https://gitlab.com/lentz/aestheticprogramming/-/blob/main/miniX5/sketch.js

*This week i got a bit confused by the assignement description as they were switched around for miniX5 and 6.
I started working on a miniX fitting not this but next weeks assignement because of that.
Luckily I realised the switch-a-roo soon enough so I was able to make this one instead.*

**The assignment** for this week was to create a generative program based on 2 rules and using at least one for loop or while function. I think what I came up with furfills these criteria. Before you look at my miniX, be advised that it has a lot of flashing colors, you should probably not look at it if your sensitive to that. I made a row of circles with for loop function that slowly and steadily moves downwards from the top of the screen. The movement is achieved by using modified code from "10print" (see source 1). On their way down the circles flash in random colors which differ in diameter. This is achieved by using the random() for RGB color values of the stroke() and for the strokeWeight(). In the center of the screen the float value of random(1) is displayed. This was done by me too show what is steering the things what are happening on screen.
In the top left corner I wrote instruction to change the frameRate, this was mainly done so that one could actually read the number shown on screen, as it runs way too fast to read normally. I do not think this goes against the rule of "no interactivity" for the miniX, as it does not change the prorams contents in any way. The speed change was implemented by two simple keyisDown() commands. One interesting thing I noted is, that the "caterpillars" move the faster when the window gets thinner on the x-axis. This makes sense, as the movement is controlled by remnants of the print10 code, which started a new line as soon as it hit the right edge of the screen.

**On the question of "Is machine autonomy given?"**, my answer would be yes and no, but more no. When I look at the for loop, I would say that it is not machine autonomy, as the program is executing a set of rules that is fully predictable. The same thing goes for classic examples in generative programming like Langtons ant by Christopher Langton(see source 2), as well as the Game of Life by John Conway(see source 3), which both work after a set of rules that theoretically could be identically recreated and foreseen by using pen and paper. The thing that makes me want to give the point of autonomy a bit of recognition is the random(1) function included in the program. I am pretty sure that the random number generator in P5 way that is not "really" random, and can probably be manipulated, as I guess it is tied to the frameRate in a way. Anyhow that is only an assumption, and for the thing I do know is that it spits out a float, I can not foresee by my knowledge as of now. That is the reason for me saying its a form of autonomy on the part of the machine. If I had to draw comparisions to some elements of Software Studies, one could say the random() variable is an algorithm in its infant stage, as there has no been fed information, furthermore it does not have the ability yet to be fed information at all, apart from the range it is tasked to pick floats from.

**It's not my favorite program that I made, but it is not the worst either.** I believe I forfilled all the requirements with it, and if thats the case it's done the job. I know from a visual standpoint the miniX is rather tiring to look of with all the flashing colors, but I think that is what makes it look like part of the neo-retro Internet aesthetic, and I like that. I would also like to mention, that this miniX was really fun to make. I did get frustrated at times, but overall the process of creation was enjoyable as I experimented a lot with it.

**Generative programs can be found all over the place**, in video games, slot machines and security systems for example. Looking at these three examples it is clear that randomness is not inherently good or bad as it can be part of something fun (games), part of something bad (gambling) and part of societies digital infrastructure (banking, passwords).

As mentioned above **randomness can be easily abused** for vile purposes like enabling gambling addictions, thereby enrichening few people on the cost of many. But that is a problem rooted in people not the program, as gambling existed already long before the program. Another randomness related subject I myself often am confronted with is so called "lootboxes" in videogames. I know this is just an interation on gambling as a whole, but I find these especially digusting as they re being put in full price titles by greedy coorperation trying to extract as much money out of their customers as possible. Further this brings down the point that randomization is a utility easily exploitable, and causes a bad impact on societies. It's not practical to forbid randomness, but it is important to prevent or at least to contain exploitation done through it.

All sources are from Winnie Soon's class and can be found under this link: https://gitlab.com/siusoon/aesthetic-programming/-/tree/master/AP2022/class06#4-10-print

Source 1: Subject 4

Source 2: Subject 11

Source 3: Subject 14

