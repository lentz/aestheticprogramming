//miniX5 - generative program
//psychedelic caterpillars by Linus Lentz
//warning!: flashing lights
//rule 1: circles move from the top the bottom of the screen
//rule 2: they stop when hitting the bottom edge and repeat


let x = 0;
let y = 0;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255);
  frameRate(20);
}

function draw() {

  if (keyIsDown(LEFT_ARROW)) {
    frameRate(1);
  }

  if (keyIsDown(RIGHT_ARROW)) {
    frameRate(20);
  }

  let s =
  textSize(20);
  textAlign(CENTER);
  text(random(1), width / 2, height / 2); //random number in the middle
  textAlign(LEFT);
  text('Left Arrow = Slow-Mo, Right Arrow = Normal', 20, 40);

  stroke(random(255), random(255), random(255));
  strokeWeight(random(75)); //something really cool happens when you change "random(75)" to just "x"

  //easteregg
  if (keyIsDown(66)) {
    strokeWeight(x);
  }


  //for loop creates the circles
  for (let x = 50; x <= width; x = x + 100) {
    fill(255, 0, 200);
    ellipse(x, y, 15, 15);
  }

  //this part makes the circles moves downwards
  x += 30;

  if (x > width) {
    x = 0;
    y += 40;
  }

  if (y > height) {
    y = 0;
  }


}
