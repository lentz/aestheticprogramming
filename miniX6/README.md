# miniX6 by Linus Lentz

![](miniX6.gif)


### Rules
###### 1. control mario with WASD or arrow key

###### 2. dodge the koopa

###### 3. catch the hat

### Game translated to use classes and OOP

###### RUNME: https://lentz.gitlab.io/aestheticprogramming/miniX6/

###### Source code: https://gitlab.com/lentz/aestheticprogramming/-/blob/main/miniX6/sketch.js

### The original game

###### RUNME: https://lentz.gitlab.io/aestheticprogramming/miniX6_extra/

###### Source code: https://gitlab.com/lentz/aestheticprogramming/-/blob/main/miniX6_extra/sketch.js


## About

**For this weeks miniX we had the assignment to use objects.** Because I already had the main part of this miniX finished due too a mixup in the assignment descriptions for this and last week, I chose too add upon it with classes. A complete rewrite would have taken more effort than making one completely from scratch, and as I was really satisfied with how this one turned out, I did not want to scrap it. In retrospective this was a good decision, as it gives a contrast between two different ways of implementing code in one example. The great thing in programming is, that there is not a right or wrong way to do it. Of course code can be written more efficient than what i did. But seeing the two ways of doing the same, or a similar task, serves as a "translation" that can help with understanding each implementation better.

My idea was to make a game with the icon of video games "Mario". I wanted a twist on the classic Mario gameplay, and therefore chose the setting were Mario lost his hat, thereby exposing his bald spot. Of can not show up to Peach's castle like this, so he has to get his hat back. But the hat is guarded by one of Marios classic enemies, a mushroom called "Goomba". Thats pretty much all there is too it. I drew the sprites and background used all from scratch in a pixel art program called "Aseprite". The music and sound effects however was lended from Super Mario Bros. for the Nintendo Entertainment system.

Like mentioned before I originally made the game using the basics of programming that I learned in the classes 1 to 6. Once I was done with the program, I transported it over to using a class based system. **The class based code does not run the same as the other one.** I got the objects to show up and to move. I understood the concept of classes and object oriented programming, as seen. An interesting new syntax I used was *dist()* which my fellow student "Jakob" showed to me. With this function it is possible to measure the distance between to points (x, y), and by combining this with an if-statement it was possible to have an interaction between objects happen. I used *noise()* functions to make the Goomba and the hat move. The Goomba makes larger movements than the hat and is always in it's proximity, this was done to make it more dangerous for Mario to get close to the hat, and achieved by sharing the *noise()* code with both and adjusting the values. Both move unpredictable, adding to the tension when the player gets closer to them. I made extensive use of imported assets such as *sounds* and *sprites*. I sorted them in extra folders to establish more clarity.

I am really satisfied with how my game turned out to be. I really so no area I could have done an improvement in it's execution, other than transferring more of the original functions to the class based sketch. I like how the game looks with its pixel aesthetic, I like how it plays, as it is hard to get the hat without the Goomba interfering, the unpredictable nature of both add to this. In a way the difficulty of the game is thereby auto-generated, and can be adjusted by simply increasing or decreasing one single value. Okay, maybe I could have improved the start, as it is possible to loose instantly as soon as the page is loaded, because the Goomba "spawned" at the same position as Mario. This would understandibly be frustrating to the player.

**Mario, the hat and the goomba are all "abstract materiality"** as Matthew Fuller would call it, refering to his conclusion on the matter of "The Obscure Objects of Object Orientation" in hist book "How to be a Geek: Essays on the Culture of Software" (Cambridge:Polity, 2017). The way the hat and Goomba behave is random to a degree not known to us before they move, even less so to the machine, as it only executes and can not approximate to their future locations like we could. Which we could with the help of the machine, at least that would make that task easier.

All in all I am very satisfied with this miniX, it was by far my biggest one yet.


Reference:

Soundtrack & Inspiration: https://www.youtube.com/watch?v=uojri3jw5mE

Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge: Polity, 2017).
